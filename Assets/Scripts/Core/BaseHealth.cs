using UnityEngine;
using UnityEngine.UIElements;
using TMPro;

public class BaseHealth : MonoBehaviour 
{
    [SerializeField] ParticleSystem hitFX;
    [SerializeField] ParticleSystem explosionFX;
    [SerializeField] TextMeshProUGUI baseHealthText;
    [SerializeField] Transform body;
    [SerializeField] float hitsPoints = 20;

    public void TakeDamage(int damage)
    {
        ProcessHits(damage);
        if (hitsPoints <= 0)
        {
            Explode();
        }

    }

    private void ProcessHits(int damage)
    {
        hitFX.Play();
        hitsPoints-= damage;
    }

    private void Explode()
    {
        ParticleSystem particle = Instantiate(explosionFX, body.position, Quaternion.identity);
        //float duration = particle.GetComponent<ParticleSystem>().main.duration;
        particle.transform.parent = transform.parent;
        // initiate death sequence 
    }
}