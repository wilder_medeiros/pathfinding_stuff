using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour 
{
    [Range(0.1f, 120f)]
    [SerializeField] float secondsBetweenSpawn = 2f;
    [SerializeField] EnemyMovement enemyPrefab;
    [SerializeField] Transform gameObjectParentTransform;

    private void Start() 
    {
        StartCoroutine(StartSpawningEnemies());
    }

    IEnumerator StartSpawningEnemies()
    {
        yield return new WaitForSeconds(secondsBetweenSpawn);
        StartCoroutine(SpawnEnemy());
    }

    IEnumerator SpawnEnemy()
    {
        while (true)
        {
            EnemyMovement enemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            enemy.transform.parent = gameObjectParentTransform;
            yield return new WaitForSeconds(secondsBetweenSpawn);
        }
    }
}