﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceableBlock : MonoBehaviour
{  
    public bool isPlaceable = true;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(isPlaceable)
            {
                FindObjectOfType<TowerFactory>().AddTower(this);
            }
            else
            {
                print("Cant place here");
            }
        }
    }

    public void SetIsPlaceable(bool modifier)
    {
        isPlaceable = modifier;
    }
}
