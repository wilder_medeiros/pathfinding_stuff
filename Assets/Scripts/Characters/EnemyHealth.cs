﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO talvez fazer um unico componente de vida 

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] ParticleSystem hitFX;
    [SerializeField] GameObject explosionFX;
    //[SerializeField] ParticleSystem implosionFX;
    [SerializeField] Transform body; 
    [SerializeField] float hitsPoints = 10;

    private void OnParticleCollision(GameObject other)
    {
        ProcessHits();
        if (hitsPoints <= 0)
        {
            KillEnemy();
        }

    }

    private void ProcessHits()
    {
        hitFX.Play();
        hitsPoints--;
    }

    private void KillEnemy()
    {
        GameObject particle = Instantiate(explosionFX, body.position, Quaternion.identity);
        //float duration = particle.GetComponent<ParticleSystem>().main.duration;
        particle.transform.parent = transform.parent;
        Destroy(gameObject);
    }
}
