﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    // variables of each tower
    [SerializeField] ParticleSystem projectileParticle;
    [SerializeField] Transform objectToPan;
    [SerializeField] float attackRange = 4f;

    public PlaceableBlock placeableBlock;

    // state of each tower
    Transform enemyTarget;

    void Update()
    {
        SetTargetEnemy();
        if (enemyTarget)
        {
            objectToPan.LookAt(enemyTarget);
            FireAtEnemy();
        }
        else
        {
            Shoot(false);
        }
    }

    private void SetTargetEnemy()
    {
        var sceneEnemies = FindObjectsOfType<EnemyHealth>();
        if (sceneEnemies.Length == 0) return;

        Transform closestEnemy = sceneEnemies[0].transform;

        foreach (var testEnemie in sceneEnemies)
        {
            closestEnemy = GetClosestEnemy(closestEnemy, testEnemie.transform);
        }

        enemyTarget = closestEnemy;
    }

    private Transform GetClosestEnemy(Transform closestEnemy, Transform testEnemy)
    {
        float closestEnemyDistance = Vector3.Distance(closestEnemy.position, transform.position);
        float testEnemyDistance = Vector3.Distance(testEnemy.position, transform.position);

        if (closestEnemyDistance < testEnemyDistance) 
        {
            return closestEnemy;
        }
        
        return testEnemy;
    }

    private void FireAtEnemy()
    {
        if (GetIsInRange())
        {
            Shoot(true);
        }
        else
        {
            Shoot(false);
        }
    }

    private void Shoot(bool isActive)
    {
        var emissionModule = projectileParticle.emission;
        emissionModule.enabled = isActive;
    }

    private bool GetIsInRange()
    {
        return Vector3.Distance(enemyTarget.position, transform.position) <= attackRange;
    }
}
