﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    // é aceitável colocar public quando a classe é de dados
    public bool isExplored = false;
    public Waypoint exploredFrom; 

    Vector2Int gridPos;
    
    const int gridSize = 10;

    public int GetGridSize()
    {
        return gridSize;
    }

    public Vector2Int GetGridPos()
    {
        gridPos.x = Mathf.RoundToInt(transform.position.x / gridSize);
        gridPos.y = Mathf.RoundToInt(transform.position.z / gridSize);

        return gridPos;
    }

    public string GetLabelText()
    {
        string labelText = gridPos.x + "," + gridPos.y;
        return labelText;
    }

    // public void SetTopColor(Color color)
    // {
    //     var topMeshRenderer = transform.Find("Top").GetComponent<MeshRenderer>();
    //     topMeshRenderer.material.color = color;
    // }
}
