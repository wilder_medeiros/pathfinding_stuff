﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] ParticleSystem implosionFX;
    [SerializeField] Transform body;
    [SerializeField] float movementPeriod = 0.5f;
    [SerializeField] int explosionDamage = 2;

    BaseHealth baseHealth; 

    void Start()
    {
        PathFinder pathFinder = FindObjectOfType<PathFinder>();
        var path = pathFinder.GetPath();
        StartCoroutine(FollowPath(path));
        baseHealth = GameObject.FindWithTag("Player").GetComponent<BaseHealth>(); 
        // maybe colocar a condição de return aqui, para ele n bugar quando destruir a base
        // TODO rever alguns desses conceitos 
    }

    private void Update() {
        if(baseHealth == null) { return; }
    }

    IEnumerator FollowPath(List<Waypoint> path)
    {
        print("Starting patrol");
        foreach (var waypoint in path)
        {
            transform.position = waypoint.transform.position;
            yield return new WaitForSeconds(movementPeriod); 
        }
        print("Finishing patrol");
        baseHealth.TakeDamage(explosionDamage);
        SelfDesctruct();
    }

    void SelfDesctruct()
    {
        ParticleSystem particle = Instantiate(implosionFX, body.position, Quaternion.identity);
        //float duration = particle.GetComponent<ParticleSystem>().main.duration;
        particle.transform.parent = transform.parent;
        Destroy(gameObject);
    }
}
