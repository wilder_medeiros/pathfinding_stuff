﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{
    [SerializeField] Tower towerPrefab;
    [SerializeField] int towerLimit = 5;
    [SerializeField] Transform gameObjectParentTransform;
    
    Queue<Tower> towersQueue = new Queue<Tower>();

    int numTowers;

    public void AddTower(PlaceableBlock placeableBlock)
    {
        numTowers = towersQueue.Count;
        if (numTowers < towerLimit)
        {
            InstantiateNewTower(placeableBlock);
        }
        else
        {
            MoveExistingTower(placeableBlock);
        }
    }

    private void InstantiateNewTower(PlaceableBlock placeableBlock)
    {
        Tower newTower = Instantiate(towerPrefab, placeableBlock.transform.position, Quaternion.identity);
        newTower.transform.parent = gameObjectParentTransform;
        placeableBlock.SetIsPlaceable(false);
        
        newTower.placeableBlock = placeableBlock; 
        numTowers++;

        towersQueue.Enqueue(newTower);
    }

    private void MoveExistingTower(PlaceableBlock placeableBlock)
    {
        print("Maximum limit tower");
        var oldTower = towersQueue.Dequeue();

        oldTower.placeableBlock.SetIsPlaceable(true); // free the old block 
        placeableBlock.SetIsPlaceable(false); // agora o que eu acabei de clicar fica falso
        
        oldTower.placeableBlock = placeableBlock; 
        oldTower.transform.position = placeableBlock.transform.position;
        
        towersQueue.Enqueue(oldTower);  
    }
}
